use std::io::{Read, Write};
use std::net::{SocketAddr, TcpListener};

use crate::{KvStore, KvsCommand, KvsEngine, KvsError, KvsResponse, Result};

pub struct KvsServer {
    engine: KvStore,
}

impl KvsServer {
    pub fn new(engine_cli: String) -> Result<Self> {
        let engine = KvStore::open(std::env::current_dir()?)?;
        Ok(KvsServer { engine })
    }

    pub fn run(&mut self, addr: SocketAddr) -> Result<()> {
        println!("Serving on: {}", addr);
        let listener = TcpListener::bind(addr)?;
        for stream in listener.incoming() {
            let mut stream = stream?;
            println!("Stream aquired from {}", stream.peer_addr()?);
            let mut buffer = [0u8; 2048];
            let read_bytes = stream.read(&mut buffer[..])?;
            println!("Read {} bytes", read_bytes);
            let request: KvsCommand = serde_json::from_slice(&buffer[0..read_bytes])?;
            println!("Request {:?}", request);
            let response = match request {
                KvsCommand::Set { key, value } => match self.engine.set(key, value) {
                    Ok(_) => KvsResponse::Set {
                        status: "OK".into(),
                    },
                    Err(e) => KvsResponse::Err {
                        error: e.to_string(),
                    },
                },
                KvsCommand::Get { key } => match self.engine.get(key) {
                    Ok(Some(value)) => KvsResponse::Get {
                        status: "OK".into(),
                        value,
                    },
                    Ok(None) => KvsResponse::Get {
                        status: "Not found".into(),
                        value: "".into(),
                    },
                    Err(e) => KvsResponse::Err {
                        error: e.to_string(),
                    },
                },
                KvsCommand::Remove { key } => match self.engine.remove(key) {
                    Ok(()) => KvsResponse::Remove {
                        status: "OK".into(),
                    },
                    Err(e) => KvsResponse::Err {
                        error: e.to_string(),
                    },
                },
            };
            println!("Response {:?}", response);
            let json = serde_json::to_string(&response)?;
            let written = stream.write(&json.into_bytes())?;
        }
        Ok(())
    }
}
