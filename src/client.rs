use std::net::{SocketAddr, TcpStream};

use crate::{KvsCommand, KvsResponse, Result};
use std::io::Write;

pub struct KvsClient {
    stream: TcpStream,
}

impl KvsClient {
    pub fn connect(addr: SocketAddr) -> Result<Self> {
        let stream = TcpStream::connect(addr)?;
        Ok(KvsClient { stream })
    }

    fn send_command(&mut self, command: KvsCommand) -> Result<KvsResponse> {
        println!("Sending command: {:?}", command);
        let json = serde_json::to_string(&command)?;
        let written = self.stream.write(&json.into_bytes())?;
        println!("Written {} bytes", written);
        let response: KvsResponse = serde_json::from_reader(&self.stream)?;
        Ok(response)
    }

    pub fn set(&mut self, key: String, value: String) -> Result<KvsResponse> {
        let command = KvsCommand::Set { key, value };
        self.send_command(command)
    }

    pub fn get(&mut self, key: String) -> Result<KvsResponse> {
        let command = KvsCommand::Get { key };
        self.send_command(command)
    }

    pub fn remove(&mut self, key: String) -> Result<KvsResponse> {
        let command = KvsCommand::Remove { key };
        self.send_command(command)
    }
}
