use serde::{Deserialize, Serialize};
use serde_json::Deserializer;

use std::collections::{BTreeMap, HashMap};
use std::fs;
use std::fs::{File, OpenOptions};
use std::io::{self, BufReader, BufWriter, Read, Seek, SeekFrom, Write};
use std::path::{Path, PathBuf};

use super::KvsEngine;
use crate::{KvsCommand, KvsError, Result};

const COMPACTION_THRESHOLD: u64 = 1024 * 1024;

type Generation = u64;
type KvsKey = String;

struct KvsCommandPosition {
    gen: Generation,
    pos: u64,
    len: u64,
}

pub struct KvStore {
    readers: HashMap<Generation, BufReader<File>>,
    writer: BufWriter<File>,
    writer_pos: u64,
    current_gen: Generation,
    index: BTreeMap<KvsKey, KvsCommandPosition>,
    path: PathBuf,
    uncompacted: u64,
}

impl KvStore {
    pub fn open<P: Into<PathBuf>>(path: P) -> Result<Self> {
        let path = path.into();
        fs::create_dir_all(&path)?;

        let mut readers = HashMap::new();
        let mut index = BTreeMap::new();

        let mut uncompacted = 0;

        let generations = log_generations(&path)?;
        for &gen in &generations {
            let mut reader = BufReader::new(File::open(log_file_path(&path, gen))?);
            uncompacted += build_index(gen, &mut reader, &mut index)?;
            readers.insert(gen, reader);
        }

        let current_gen = generations.iter().max().unwrap_or(&0) + 1;
        let mut writer = new_log_file(&path, current_gen, &mut readers)?;
        let writer_pos = writer.seek(SeekFrom::Start(0))?;
        Ok(KvStore {
            readers,
            writer,
            writer_pos,
            current_gen,
            index,
            path,
            uncompacted,
        })
    }

    pub fn compact(&mut self) -> Result<()> {
        let compaction_gen = self.current_gen + 1;
        let mut compaction_writer = new_log_file(&self.path, compaction_gen, &mut self.readers)?;

        self.current_gen += 2;
        self.writer = new_log_file(&self.path, self.current_gen, &mut self.readers)?;
        self.writer_pos = self.writer.seek(SeekFrom::Start(0))?;

        let mut new_pos = 0;
        for cmd_pos in &mut self.index.values_mut() {
            let reader = self
                .readers
                .get_mut(&cmd_pos.gen)
                .expect("Could not find reader for generation");
            reader.seek(SeekFrom::Start(cmd_pos.pos))?;
            let mut entry = reader.take(cmd_pos.len);
            let len = io::copy(&mut entry, &mut compaction_writer)?;
            *cmd_pos = KvsCommandPosition {
                gen: compaction_gen,
                pos: new_pos,
                len,
            };
            new_pos += len;
        }
        compaction_writer.flush()?;

        let stale_gens: Vec<_> = self
            .readers
            .keys()
            .filter(|&&gen| gen < compaction_gen)
            .cloned()
            .collect();
        for gen in stale_gens {
            self.readers.remove(&gen);
            std::fs::remove_file(log_file_path(&self.path, gen))?;
        }
        Ok(())
    }
}

impl KvsEngine for KvStore {
    fn set(&mut self, key: String, value: String) -> Result<()> {
        let json_command = serde_json::to_string(&KvsCommand::Set {
            key: key.clone(),
            value,
        })?;
        let pos = self.writer_pos;
        let len = self.writer.write(json_command.as_bytes())? as u64;
        if let Some(old_cmd) = self.index.insert(
            key,
            KvsCommandPosition {
                gen: self.current_gen,
                pos,
                len,
            },
        ) {
            self.uncompacted += old_cmd.len;
        }
        self.writer.flush()?;
        self.writer_pos += len;
        if self.uncompacted > COMPACTION_THRESHOLD {
            self.compact()?;
        }
        Ok(())
    }

    fn get(&mut self, key: String) -> Result<Option<String>> {
        match self.index.get(&key) {
            Some(command) => {
                let reader = self
                    .readers
                    .get_mut(&command.gen)
                    .expect("Cannot find log reader");
                reader.seek(SeekFrom::Start(command.pos))?;
                let command_reader = reader.take(command.len);
                if let KvsCommand::Set { value, .. } = serde_json::from_reader(command_reader)? {
                    Ok(Some(value))
                } else {
                    Err(KvsError::UnexpectedCommand)
                }
            }
            None => Ok(None),
        }
    }

    fn remove(&mut self, key: String) -> Result<()> {
        if let Some(old_cmd) = self.index.remove(&key) {
            let json_command = serde_json::to_string(&KvsCommand::Remove { key })?;
            let len = self.writer.write(json_command.as_bytes())? as u64;
            self.writer_pos += len;
            self.writer.flush()?;
            self.uncompacted += old_cmd.len;
            Ok(())
        } else {
            Err(KvsError::KeyNotFound)
        }
    }
}

fn log_generations<P: AsRef<Path>>(path: P) -> Result<Vec<Generation>> {
    let mut log_generations: Vec<Generation> = fs::read_dir(path)?
        .filter_map(|entry| entry.ok().and_then(|e| Some(e.path())))
        .filter(|path| path.is_file() && path.extension() == Some("log".as_ref()))
        .flat_map(|path| {
            path.file_name()
                .and_then(std::ffi::OsStr::to_str)
                .map(|s| s.trim_end_matches(".log"))
                .map(str::parse::<Generation>)
        })
        .flatten()
        .collect();
    // this needs to be sorted to get the logs read in the right order and keep consistency at
    // the same time of the index
    log_generations.sort_unstable();
    Ok(log_generations)
}

fn build_index(
    gen: Generation,
    reader: &mut BufReader<File>,
    index: &mut BTreeMap<KvsKey, KvsCommandPosition>,
) -> Result<u64> {
    let mut uncompacted = 0;
    let mut pos = reader.seek(SeekFrom::Start(0))?;
    let mut stream = Deserializer::from_reader(reader).into_iter::<KvsCommand>();
    while let Some(cmd) = stream.next() {
        let newpos = stream.byte_offset() as u64;
        let len = newpos - pos;
        match cmd? {
            KvsCommand::Set { key, .. } => {
                if let Some(old_cmd) = index.insert(key, KvsCommandPosition { gen, pos, len }) {
                    uncompacted += old_cmd.len;
                }
            }
            KvsCommand::Remove { key } => {
                if let Some(old_cmd) = index.remove(&key) {
                    uncompacted += old_cmd.len;
                }
                uncompacted += len;
            }
            _ => {}
        }
        pos = newpos;
    }
    Ok(uncompacted)
}

fn new_log_file<P: AsRef<Path>>(
    path: P,
    gen: Generation,
    readers: &mut HashMap<Generation, BufReader<File>>,
) -> Result<BufWriter<File>> {
    let log_file_path = log_file_path(&path, gen);
    let writer = BufWriter::new(
        OpenOptions::new()
            .create(true)
            .append(true)
            .open(&log_file_path)?,
    );
    readers.insert(gen, BufReader::new(File::open(&log_file_path)?));

    Ok(writer)
}

fn log_file_path<P: AsRef<Path>>(path: P, gen: Generation) -> PathBuf {
    path.as_ref().join(format!("{}.log", gen))
}
