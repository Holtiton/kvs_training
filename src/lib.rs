use serde::{Deserialize, Serialize};

pub use client::KvsClient;
pub use engines::{KvStore, KvsEngine};
pub use error::{KvsError, Result};
pub use server::KvsServer;

mod client;
mod engines;
mod error;
mod server;

#[derive(Serialize, Deserialize, Debug)]
pub enum KvsCommand {
    Set { key: String, value: String },
    Get { key: String },
    Remove { key: String },
}

#[derive(Serialize, Deserialize, Debug)]
pub enum KvsResponse {
    Set { status: String },
    Get { status: String, value: String },
    Remove { status: String },
    Err { error: String },
}
