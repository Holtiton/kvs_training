use structopt::StructOpt;

use kvs::{KvsServer, Result};
use std::net::SocketAddr;

#[derive(StructOpt)]
struct KvsServerOpt {
    #[structopt(
        long,
        name = "addr",
        value_name = "IP:PORT",
        default_value = "127.0.0.1:4000"
    )]
    address: SocketAddr,
    #[structopt(long, name = "eng", default_value = "kvs")]
    engine: String,
}

fn main() -> Result<()> {
    match KvsServerOpt::from_args() {
        KvsServerOpt {
            address: addr,
            engine: eng,
        } => {
            let mut server = KvsServer::new(eng)?;
            server.run(addr)?;
        }
    }
    Ok(())
}
