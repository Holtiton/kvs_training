use structopt::StructOpt;

use std::env::current_dir;
use std::net::SocketAddr;

use kvs::{KvsClient, KvsError, Result};

#[derive(StructOpt)]
enum KvsClientOpt {
    #[structopt(name = "get")]
    Get {
        key: String,
        #[structopt(
            long,
            name = "addr",
            default_value = "127.0.0.1:4000",
            parse(try_from_str)
        )]
        addr: SocketAddr,
    },
    #[structopt(name = "set")]
    Set {
        key: String,
        value: String,
        #[structopt(
            long,
            name = "addr",
            default_value = "127.0.0.1:4000",
            parse(try_from_str)
        )]
        addr: SocketAddr,
    },
    #[structopt(name = "rm")]
    Remove {
        key: String,
        #[structopt(
            long,
            name = "addr",
            default_value = "127.0.0.1:4000",
            parse(try_from_str)
        )]
        addr: SocketAddr,
    },
}

fn main() -> Result<()> {
    match KvsClientOpt::from_args() {
        KvsClientOpt::Get { key, addr } => {
            let mut client = KvsClient::connect(addr)?;
            let response = client.get(key)?;
            println!("{:?}", response);
        }
        KvsClientOpt::Set { key, value, addr } => {
            let mut client = KvsClient::connect(addr)?;
            let response = client.set(key, value)?;
            println!("{:?}", response);
        }
        KvsClientOpt::Remove { key, addr } => {
            let mut client = KvsClient::connect(addr)?;
            let response = client.remove(key)?;
            println!("{:?}", response);
        }
    }
    Ok(())
}
