use std::error::Error;
use std::fmt;
use std::io;

use serde_json;

#[derive(Debug)]
pub enum KvsError {
    Io(io::Error),
    Serialize(serde_json::error::Error),
    KeyNotFound,
    UnexpectedCommand,
}

impl fmt::Display for KvsError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            KvsError::Io(ref err) => write!(f, "IO error: {}", err),
            KvsError::Serialize(ref err) => write!(f, "Serialization error: {}", err),
            KvsError::KeyNotFound => write!(f, "Key not found"),
            KvsError::UnexpectedCommand => write!(f, "Unexpected command"),
        }
    }
}

impl Error for KvsError {
    fn description(&self) -> &str {
        match *self {
            KvsError::Io(ref err) => err.description(),
            KvsError::Serialize(ref err) => err.description(),
            KvsError::KeyNotFound => "Key not found",
            KvsError::UnexpectedCommand => "Unexpected command",
        }
    }

    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match *self {
            KvsError::Io(ref err) => Some(err),
            KvsError::Serialize(ref err) => Some(err),
            _ => None,
        }
    }
}

impl From<io::Error> for KvsError {
    fn from(err: io::Error) -> KvsError {
        KvsError::Io(err)
    }
}

impl From<serde_json::error::Error> for KvsError {
    fn from(err: serde_json::error::Error) -> KvsError {
        KvsError::Serialize(err)
    }
}

pub type Result<T> = std::result::Result<T, KvsError>;
